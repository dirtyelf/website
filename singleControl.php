<?php
session_start();
if (!isset($_SESSION['user_id']) || !$_SESSION['user_active']) {
    header("Location: index.php");
}

if (!($_SESSION['user_role'] == 'admin' || $_SESSION['user_role'] == 'resident')) {
    header("Location: index.php?forbid=homeControl");
 }

 include 'includes/pageVisit-inc.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>All Control</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/style.css">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <!-- Action Alert -->
    <div id="actionAlert" class="alert alert-info" style="display:none;">
        <button id="closeAlert" type="button" class="close">&times;</button>
        <strong id="alertMsg">No button was pressed.</strong>
        <div id="responseMsg">There is no response.</div>
    </div>

    <!-- Main Content Here -->
    <div class="container-fluid my-2">
        <div class="">Master</div>
        <div class="btn-group btn-group-sm">
            <button name="light-on-off" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_light-on-off')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-lightbulb fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
            <button name="light-dim" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_light-dim')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-lightbulb fa-stack-1x" style="color:#D0D0D0"></i>
                </span>
            </button>
            <button name="fan-off" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_fan-off')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-wind fa-stack-1x" style="color:#808080"></i>
                    <i class="fas fa-ban fa-stack-2x" style="color:#D0D0D0"></i>
                </span>
            </button>
            <button name="fan-low" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_fan-low')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-wind fa-stack-1x" style="color:#D0D0D0"></i>
                </span>
            </button>
            <button name="fan-med" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_fan-med')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-wind fa-stack-1x" style="color:#808080"></i>
                </span>
            </button>
            <button name="fan-hi" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_fan-high')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-wind fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
            <button name="hb-mode" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_hb-mode')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-eye fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
            <button name="night" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('mb_night-time')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-moon fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
        </div>
        <div class="">Living</div>
        <div class="btn-group btn-group-sm">
            <button name="light-on-off" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_light-on-off')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-lightbulb fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
            <button name="light-dim" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_light-dim')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-lightbulb fa-stack-1x" style="color:#D0D0D0"></i>
                </span>
            </button>
            <button name="fan-off" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_fan-off')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-wind fa-stack-1x" style="color:#808080"></i>
                    <i class="fas fa-ban fa-stack-2x" style="color:#D0D0D0"></i>
                </span>
            </button>
            <button name="fan-low" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_fan-low')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-wind fa-stack-1x" style="color:#D0D0D0"></i>
                </span>
            </button>
            <button name="fan-med" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_fan-med')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-wind fa-stack-1x" style="color:#808080"></i>
                </span>
            </button>
            <button name="fan-hi" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_fan-high')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-wind fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
            <button name="tv-mode" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('lr_tv-mode')">
                <span class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i class="fas fa-tv fa-stack-1x" style="color:#303030"></i>
                </span>
            </button>
        </div>
        <div class="">Foyer</div>
        <div class="btn-group btn-group-sm">
            <button name="light-on-off" class="btn btn-dark my-2" style="font-size: 5px" onclick="callSwitch('foyer_light-on-off')">
                <span class="fa-stack fa-2x" style="color:#000">
                    <i class="fas fa-square fa-stack-2x"></i>
                    <i id="lightBulb" class="fas fa-lightbulb fa-stack-1x" style="color:#737373"></i>
                </span>
            </button>
        </div>
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include 'common/jumbotronbot.php'; ?>

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="/scripts/allControl.js"></script>

</body>

</html>