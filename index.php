<?php
session_start();

include 'includes/pageVisit-inc.php';

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Home Sweet 8008</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">

</head>

<body>

    <!-- Include the Header Jumbotron -->
    <?php include 'common/jumbotrontop.php'; ?>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <!-- Dismissible Login Alerts -->
    <?php if (isset($_GET['login']) && $_GET['login']=="success"): ?>
    <div class="alert alert-success alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Success!</strong> Thank you for logging in. Check out your <a href="profile.php" class="alert-link">profile</a>.
    </div>
    <?php elseif (isset($_GET['login']) && $_GET['login']=="fail"): ?>
    <div class="alert alert-warning alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oops!</strong> Check your password.
    </div>
    <?php elseif (isset($_GET['login']) && $_GET['login']=="empty"): ?>
    <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oops!</strong> Please enter a username and password.
    </div>
    <?php elseif (isset($_GET['login']) && $_GET['login']=="notFound"): ?>
    <div class="alert alert-warning alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oops!</strong> Username not found.
    </div>
    <?php elseif (isset($_GET['signup']) && $_GET['signup']=="success"): ?>
    <div class="alert alert-primary alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Almost there!</strong> Check your email to complete your registration.
    </div>
    <?php elseif (isset($_GET['forbid']) && $_GET['forbid']=="homeControl"): ?>
    <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Forbidden!</strong> You do not have access to the home control.
    </div>
    <?php else: ?>
    <!-- No Alert -->
    <?php endif; ?>

    <!-- Main Site Home Display -->
    <div class="container my-sm-3">
        Hello from Lindsay and Bryan!
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include 'common/jumbotronbot.php'; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>

</body>

</html>