<?php

require 'includes/db-inc.php';
session_start();

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    if (isset($_POST['email']) && !empty($_POST['email']) && 
        isset($_POST['hash']) && !empty($_POST['hash']) &&
        isset($_POST['pw']) && !empty($_POST['pw']) && 
        isset($_POST['pwc']) && !empty($_POST['pwc'])) {

            $email = mysqli_real_escape_string($connection, $_POST['email']);
            $hash = mysqli_real_escape_string($connection, $_POST['hash']);
            $newPW = mysqli_real_escape_string($connection, $_POST['pw']);
            $chkPW = mysqli_real_escape_string($connection, $_POST['pwc']);

            if ($newPW == $chkPW) {
                $hashedPW = password_hash($newPW, PASSWORD_DEFAULT);
                $sql = "UPDATE users SET user_pwd='$hashedPW' WHERE user_email='$email' AND user_hash='$hash'";
                $result = mysqli_query($connection, $sql);
                if ($result) {
                    header("Location: index.php?reset=success");
                    exit();
                } else {
                    header("Location: index.php?reset=dberror");
                    exit();
                }
            } else {
                header("Location: index.php?reset=noMatch");
                exit();
            }

        } else {
            header("Location: index.php");
            exit();
        }
    } elseif ($_SERVER['REQUEST_METHOD'] == "GET") {

    if(isset($_GET['email']) && !empty($_GET['email']) && isset($_GET['hash']) && !empty($_GET['hash'])) {
        // check to see if the email/hash vars are empty
        $email = mysqli_real_escape_string($connection, $_GET['email']);
        $hash = mysqli_real_escape_string($connection, $_GET['hash']);
    
        // find user with matching email and hash
        $sql = "SELECT * FROM users WHERE user_email='$email' AND user_hash='$hash'";
        $result = mysqli_query($connection, $sql);
        $resultCheck = mysqli_num_rows($result);
    
        if($resultCheck == 0) {
            
            echo('User not found.');
        
        } else {
    
            echo('
            
            <!DOCTYPE html>
            <html>
            <head>
                <meta charset="utf-8" />
                <title>Forgot PW</title>
            </head>
            <body>
    
                <form action="reset.php" method="post">
                    <input type="password" name="pw" placeholder="password"><br>
                    <input type="password" name="pwc" placeholder="confirm"><br>
                    <input type="hidden" name="email" value="' . $email . '">
                    <input type="hidden" name="hash" value="' . $hash . '">
                    <button type="submit" name="reset">reset</button>
                </form>
    
                <br>
                <a href="index.php">Home</a>
    
            </body>
            </html>
            
            ');
    
            
        }
    } else {
        header("Location: index.php");
        exit();
    }

} else {
    header("Location: index.php");
    exit();
}

?>