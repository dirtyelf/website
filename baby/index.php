<?php

session_start();

if (!isset($_SESSION['user_id']) || !$_SESSION['user_active']) {
    header("Location: /index.php");
}

if (!($_SESSION['user_role'] == 'admin' || $_SESSION['user_role'] == 'resident')) {
    header("Location: /index.php");
 }

include $_SERVER['DOCUMENT_ROOT'] . '/includes/pageVisit-inc.php';

$tableColumns = mysqli_query($connection, "SHOW columns FROM makenzie");

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Baby M</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include '../common/navbar.php'; ?>

    <!-- Baby Main Display -->
    <div class="container my-sm-3">
        <div class="row my-sm-3">
            <div class="col-12">
                <canvas id="mChart" width="800" height="400"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-12" id="chtControls">
                <div class="dropdown my-3">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="primaryAxis" data-toggle="dropdown">click here to show data</button>
                    <div class="dropdown-menu">
                        <?php 
                        while ($column = mysqli_fetch_assoc($tableColumns)):
                            if($column['Field'] != 'id'): 
                                if($column['Field'] != 'date'):
                                    if($column['Field'] != 'addedBy'):
                        ?>
                        <button class="dropdown-item" id="<?php echo $column['Field']; ?>Select"><?php echo $column['Field']; ?></button>
                        <?php
                                    endif;
                                endif;
                            endif; 
                        endwhile;
                        mysqli_free_result($tableColumns); 
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#formCollapse" aria-expanded="false" aria-controls="formCollapse">
                    click here to add data
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="collapse" id="formCollapse">
                    <form id="addDataPoint">
                        <?php 
                $url = $_SERVER['SERVER_NAME'] . '/includes/db-process.php?entryForm=makenzie&notdba=yes&hash=' . $_SESSION['user_hash'];
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                $response = curl_exec($curl);
                echo $response;
                curl_close($curl);
                ?>
                    </form>
                </div>
            </div>
        </div>

        <!-- Action Alert -->
        <div id="actionAlert" class="alert alert-info" style="display:none;">
            <button id="closeAlert" type="button" class="close">&times;</button>
            <div id="alertMsg">No action.</div>
            <strong id="responseMsg">No message from the server.</strong>
        </div>

        <div class="row mt-5">
            <div class="col">
                <p class="text-center">Check out her name guess game from before she was born!</p>
                <p class="text-center"><a href="nameguess.php" class="btn btn-info" role="button">Play Now!</a></p>
            </div>
        </div>
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include '../common/jumbotronbot.php'; ?>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"
        integrity="sha512-0QbL0ph8Tc8g5bLhfVzSqxe9GERORsKhIn1IrpxDAgUsbBGz/V7iSav2zzW325XGd1OMLdL4UiqRJj702IeqnQ==" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js" integrity="sha512-rmZcZsyhe0/MAjquhTgiUcb4d9knaFc7b5xAfju483gbEXTkeJRUMIPk6s3ySZMYUHEcjKbjLjyddGWMrNEvZg=="
        crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg=="
        crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.2/dist/chart.min.js" integrity="sha256-qoN08nWXsFH+S9CtIq99e5yzYHioRHtNB9t2qy1MSmc=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@0.1.1"></script>
    <script src="/scripts/dba.js"></script>
    <script src="/baby/scripts/baby-chart.js"></script>
</body>

</html>