<?php
session_start();
?>

<!DOCTYPE <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Baby Auz April 2019</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="styles/baby-styles.css">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <!-- Main Site Home Display -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card text-center text-white bg-secondary mt-3 mb-3">
                    <div class="card-header">Name Guess Game</div>
                    <div class="card-body">
                        <div class="hangman">
                            <div class="guess"></div>
                            <form class="guessForm" autocomplete="off">
                                <input type="text" class="guessLetter" id="guessBox" maxlength="1" placeholder="Enter a letter . . . &#x23ce;" />
                                <button type="submit" class="guessButton">Guess</button>
                            </form>
                            <p>Hint: It's a girl! <br /></p>
                            <div class="wrong">
                                <div class="wrongLetters"></div>
                            </div>
                            <div class="message">
                                <ul style="padding: 0px; margin: 0px;">
                                    <li class="lastName">A</li>
                                    <li class="lastName">U</li>
                                    <li class="lastName">Z</li>
                                </ul>
                                <h1 class="title"></h1>
                                <p class="text"></p>
                                <button class="restart button" id="rstBtn">Play Again?</button>
                                <p class="gbLink"></p>
                            </div>
                        </div>
                    </div>
                    <a href="/baby/index.php" class="btn btn-primary">Home</a>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include '../common/jumbotronbot.php'; ?>

    <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="/baby/scripts/baby-guess.js"></script>

</body>

</html>