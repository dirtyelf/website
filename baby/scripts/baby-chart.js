var data = {
    labels: [],
    datasets: [
        {
            spanGaps: true,
            label: '',
            borderColor: 'rgb(255,99,132)',
            backgroundColor: 'rgb(255,99,132)',
            fill: false,
            lineTension: .2,
            data: [],
        } 
    ]
}; 
var options = {
    scales: 
    {
        x: {
            type: 'time',
            time: {
                unit: 'day'
            },
            display: true,
            },
    }
};
var config = 
{
    type:'line',
    data,
    options
};

//jquery on load
$(function(){
    var mChart = new Chart($('#mChart'), config);

    $('#addDataPoint').on('click', '#makenzieSave', function(){
        // get all the fields key->value
        var tableToEdit = 'makenzie';
        var xhttp;
        var inputs = $('#addDataPoint').find('.input-group');
        var formData = json_data_form_inputs(inputs);
        var data = JSON.stringify(`{"table":"${tableToEdit}", "data":${formData}}`);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            alert_message(`Attempted to add new entry to ${tableToEdit}.`, $.trim(this.responseText), 3500);
            $('#addDataPoint')[0].reset();
            $('#addDataPoint').find(".input-group").removeClass("edited");
            $('#addDataPoint').find(".input-group-append").remove();
        }
        };
        xhttp.open("POST", `/includes/db-process.php`, true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(data);
    });

    $('.dropdown-menu button').on('click', async function(){
        //change chart data
        let chart_data = await json_data_chart_dataset("date, " + this.innerHTML);
        let new_data = [];
        let new_labels = [];
        chart_data.forEach((v) => {    
            new_data.push(v[1]);
            new_labels.push(moment(v[0]));
            return;
            });
        mChart.data.datasets[0].data = new_data;
        mChart.data.datasets[0].label = this.innerHTML;
        mChart.data.labels = new_labels;
        mChart.update();
    });

    $('#addDataPoint').on('input', '.input-group', function(){
        if (!$(this).hasClass('edited')){
            var editedAppend=$('<div />').addClass('input-group-append');
            var editedSpan=$('<span />').addClass('input-group-text').html('Edited');
            $(this).append(editedAppend);
            editedAppend.html(editedSpan);
            $(this).addClass('edited');
        } else {
            if ($(this).children('input')[0].value == ''){
                $(this).removeClass('edited');
                $(this).children('.input-group-append').remove();
            }
        }
    }); 
});