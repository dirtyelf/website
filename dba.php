<?php
session_start();

require 'includes/db-inc.php';

if ($_SESSION['user_role'] != 'admin') {
    header("Location: index.php");
}

include 'includes/pageVisit-inc.php';

$DBtables = mysqli_query($connection, "SHOW TABLES");

if (isset($_GET['table'])) {
    $activeTable = $_GET['table'];
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Database Admin</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <!-- Action Alert -->
    <div id="actionAlert" class="alert alert-info" style="display:none;">
        <button id="closeAlert" type="button" class="close">&times;</button>
        <div id="alertMsg">No action.</div>
        <strong id="responseMsg">No message from the server.</strong>
    </div>

    <!-- Main Display -->
    <div class="container-fluid my-3">
        <div class="row align-items-start mx-2">
            <div class="col-sm-4">
                <h3>Database Admin</h3>
            </div>
            <div class="col-sm-8 align-self-start">
                <div class="btn-group">
                    <div class="btn-group">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="tableSelect" data-toggle="dropdown">Select Table</button>
                        <div class="dropdown-menu">
                            <?php while ($table = mysqli_fetch_row($DBtables)): ?>
                            <button class="dropdown-item" id="<?php echo $table[0]; ?>Select"><?php echo $table[0]; ?></button>
                            <?php 
                        endwhile;
                        mysqli_free_result($DBtables); 
                        ?>
                        </div>
                    </div>
                    <button class="btn btn-success" id="addBtn" data-id="new">Add Record</button>
                </div>
            </div>
        </div>
        <div id="dbTable"></div>
    </div>

    <!-- DB Entry Modal -->
    <div class="modal fade" id="entryModal" tabindex="-1" role="dialog" aria-labelledby="entryModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="entryModalLabel">Edit DB Entry</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="entryForm">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="entryModalSave">Save Edits</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include 'common/jumbotronbot.php'; ?>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"
        integrity="sha512-0QbL0ph8Tc8g5bLhfVzSqxe9GERORsKhIn1IrpxDAgUsbBGz/V7iSav2zzW325XGd1OMLdL4UiqRJj702IeqnQ==" crossorigin="anonymous"></script>
    <script src="/scripts/dba.js"></script>

</body>

</html>