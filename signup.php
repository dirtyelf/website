<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Register</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <!-- Dismissible Signup Alerts -->
    <?php if (isset($_GET['signup']) && $_GET['signup']=="reCaptcha"): ?>
    <div class="alert alert-warning alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oops!</strong> Please fill out the reCaptcha!
    </div>
    <?php else: ?>
    <!-- No Alert -->
    <?php endif; ?>

    <!-- Main Register Page Display -->
    <div class="container mt-3">
        <form action="signup-disabled.php" method="get">
            <div class="form-row">
                <div class="form-group col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Name</span>
                        </div>
                        <input type="text" class="form-control" name="firstName" placeholder="first">
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Name</span>
                        </div>
                        <input type="text" class="form-control" name="lastName" placeholder="last">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">User Name</span>
                        </div>
                        <input type="text" class="form-control" name="userName" placeholder="user name">
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">E-Mail</span>
                        </div>
                        <input type="email" class="form-control" name="email" placeholder="email">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Password</span>
                        </div>
                        <input type="password" class="form-control" name="pwd" placeholder="password">
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Confirm</span>
                        </div>
                        <input type="password" class="form-control" name="pwdC" placeholder="confirm password">
                    </div>
                </div>
            </div>
            <div class="btn-group mb-3">
                <button type="submit" class="btn btn-primary" name="submit">Register</button>
                <a class="btn btn-secondary" href="index.php" role="button">Home</a>
            </div>
            <div class="g-recaptcha my-3" data-sitekey="6Lf6O3MUAAAAAINUdF6T2xW72m4mXabx7_jMxWLU"></div>
        </form>
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include 'common/jumbotronbot.php'; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

</body>

</html>