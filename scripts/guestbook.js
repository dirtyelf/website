function likePost(postID) {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            document.getElementById("numLikes").innerHTML = this.responseText;
        }
    };
    xhttp.open("POST", "../includes/guestbook-inc.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send("l="+postID);
}