$(function(){
    
    $('.dropdown-menu button').on('click', function(){
        var tblName = this.innerHTML;
        xhttp_getTable(tblName);
        $('#tableSelect').text(tblName);
    });
    
    $('#dbTable').on('click', '.deleteRow', function(){
        var xhttp;
        var rowToDel = this.id;
        var rowClicked = $(this).closest('tr');
        var tableToDelFrom = $('#tableSelect').html();
        var data = JSON.stringify(`{"table":"${tableToDelFrom}", "id":${rowToDel}}`);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                if(this.responseText == 'Nothing was deleted.'){
                    alert_message(`Attempted to delete ${rowToDel} from ${tableToDelFrom}.`, $.trim(this.responseText), 3500);
                }
                else {
                    alert_message(`Attempted to delete ${rowToDel} from ${tableToDelFrom}.`, $.trim(this.responseText), 3500);
                    rowClicked.children().fadeOut(500).promise().done(function(){
                        rowClicked.remove();
                    });
                }
            }
        };
        xhttp.open("DELETE", `/includes/db-process.php`, true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(data);
    });
    
    $('#addBtn').on('click', function(){
        var tableToAddTo = $('#tableSelect').html();
        if (tableToAddTo == 'Select Table'){
            alert_message('No table selected.', 'Select a table before adding a record.', 1500);
        }
        else {
            $('#entryModal').modal('show', $('#addBtn'));
        }
    });
   
   $(document).on('show.bs.modal', '#entryModal' , function(event){
        var getURL = '';
        var tableToEdit = $('#tableSelect').html();
        var idToEdit = $(event.relatedTarget).data('id');
        if (idToEdit == 'new'){
            $('#entryModalLabel').html('Add New Record');
            $('#entryModalSave').data('add', true);
            getURL = `/includes/db-process.php?entryForm=${tableToEdit}`;
        }
        else {
            $('#entryModalLabel').html('Edit DB Record');
            $('#entryModalSave').data('add', false);
            getURL = `/includes/db-process.php?entryForm=${tableToEdit}&id=${idToEdit}`;
        }
        // ajax call to get form for modal
        var xhttp;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                $('#entryForm').html(this.response);
                if (idToEdit != 'new'){
                    $('#entryModalSave').data('id', idToEdit);
                }
                $('#entryForm').find('input').attr('autocomplete', 'off');
                $('#entryModal').modal('handleUpdate');
             }
         };
         xhttp.open("GET", getURL, true);
         xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
         xhttp.send();
    });
    
    $('#entryModal').on('click', '#entryModalSave', function(){
        // get all the fields key->value
        var tableToEdit = $('#tableSelect').html();
        var addingNewRecord = $('#entryModalSave').data('add');
        var xhttp;
        if (!addingNewRecord){
            var rowToEdit = $('#entryModalSave').data('id');
            var priKey = $('#entryForm').find('input:disabled').attr('id');
            var inputs = $('#entryForm').find('.input-group.edited');
            var formData = json_data_form_inputs(inputs);
            var data = JSON.stringify(`{"table":"${tableToEdit}", "key":"${priKey}", "id":${rowToEdit}, "data":${formData}}`);
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if(this.readyState == 4 && this.status == 200) {
                    $('#entryModal').modal('hide');
                    alert_message(`Attempted to edit ${rowToEdit} from ${tableToEdit}.`, $.trim(this.responseText), 3500)
                    xhttp_getTable(tableToEdit);
                }
            };
            xhttp.open("PUT", `/includes/db-process.php`, true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send(data);
        }
        else {
            var inputs = $('#entryForm').find('.input-group');
            var formData = json_data_form_inputs(inputs);
            var data = JSON.stringify(`{"table":"${tableToEdit}", "data":${formData}}`);
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                $('#entryModal').modal('hide');
                alert_message(`Attempted to add new entry to ${tableToEdit}.`, $.trim(this.responseText), 3500)
                xhttp_getTable(tableToEdit);
            }
            };
            xhttp.open("POST", `/includes/db-process.php`, true);
            xhttp.setRequestHeader("Content-Type", "application/json");
            xhttp.send(data);
        }
    });
    
    $('#entryForm').on('input', '.input-group', function(){
        if (!$(this).hasClass('edited')){
            var editedAppend = $('<div />').addClass('input-group-append');
            var editedSpan = $('<span />').addClass('input-group-text').html('Edited');
            $(this).append(editedAppend);
            editedAppend.html(editedSpan);
            $(this).addClass('edited');
        } else {
            if ($(this).children('input')[0].value == ''){
                $(this).removeClass('edited');
                $(this).children('.input-group-append').remove();
            }
        }
    });  
});

function xhttp_getTable(tbl){
  var xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if(this.readyState == 4 && this.status == 200) {
          $('#dbTable').html(this.response);
      }
  };
  xhttp.open("GET", `/includes/db-process.php?table=${tbl}`, true);
  xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhttp.send();
}

async function xhttp_getTableCol(tbl, col, sort){
    let url = "";
    if(sort){
        url = `/includes/db-process.php?table=${tbl}&col=${col}&sort=${sort}`;
    } else {
        url = `/includes/db-process.php?table=${tbl}&col=${col}`;
    }
    try{
        let response = await fetch(url);
        return await response.json();
    } catch(error){
        console.error(error);
    }
  }

function json_data_form_inputs(input_data_arr){
  var formData = '[';
  input_data_arr.each((index, input) => { 
      var colKey = $(input).children('.input-group-prepend').children('span').html();
      var colValue = $(input).children('input').val();
      formData += '{"';
      formData += colKey;
      formData += '":"';
      if (colValue){
        formData += colValue;
      } else {
          formData += "NULL";
      }
      if (index == input_data_arr.length - 1) {
          formData += '"}';
      }
      else {
          formData += '"},';
      }
  });
  formData += ']';
  return formData;
}

function json_data_chart_dataset(col){
    return xhttp_getTableCol('makenzie', col, 'date');
}

function alert_message(msg, resp, delay){
  $("#alertMsg").html(msg);
  $("#responseMsg").html(resp);
  $("#actionAlert").addClass("active");
  $("#actionAlert").slideDown(500, 'easeInOutQuint', function(){
      $("#actionAlert").delay(delay).slideUp(500, 'easeInOutQuint', function(){
          $("#alertMsg").html('No Action.');
          $("#responseMsg").html('No message from the server.');
      });
  });
}