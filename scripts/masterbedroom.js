function callSwitch(btnID) {
    var alertMsg = "";
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            $("#alertMsg").html(alertMsg);
            $("#responseMsg").html($.trim(this.responseText));
            $("#actionAlert").addClass("active");
            $("#actionAlert").slideDown(500, 'easeInOutQuint', function(){
                $("#actionAlert").delay(2500).slideUp(500, 'easeInOutQuint');
            });
        }
    };
    xhttp.open("POST", "../includes/masterbed-inc.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    switch (btnID) {
        case "light-on-off":
            alertMsg = "The light on-off button was pressed.";
            xhttp.send("light=on-off");
            break;
        case "light-dim":
            alertMsg = "The light dim button was pressed.";
            xhttp.send("light=dim");
            break;
        case "fan-off":
            alertMsg = "The fan off button was pressed.";
            xhttp.send("fan=off");
            break;
        case "fan-low":
            alertMsg = "The fan low button was pressed.";
            xhttp.send("fan=low");
            break;
        case "fan-med":
            alertMsg = "The fan medium button was pressed.";
            xhttp.send("fan=medium");
            break;
        case "fan-high":
            alertMsg = "The fan high button was pressed.";
            xhttp.send("fan=high");
            break;
        case "hb-mode":
            alertMsg = "The heartbeat button was pressed.";
            xhttp.send("hbMode=on-off");
            break;
        case "night-time":
            alertMsg = "Night mode engaged.";
            xhttp.send("night=on-off");
            break;
        default:
            alertMsg = "There was an error.";
            break;
    }
}

$("#closeAlert").click(function() {
    $("#actionAlert").clearQueue();
    $("#actionAlert").slideUp(500, 'easeInOutQuint');
    }); 