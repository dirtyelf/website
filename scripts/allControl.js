$(document).ready(updateState);

function updateState() {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            //set the color of the lightbulb appropriately
            if(this.responseText == "on") {
                $("#lightBulb").css("color", "#fbff0f")
            } else {
                $("#lightBulb").css("color", "#737373")
            }
        }
    };
    xhttp.open("POST", "../includes/foyer-inc.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send("light=request");
}

function callSwitch(btnID) {
    var alertMsg = "";
    var room = btnID.split("_")[0];
    var action = btnID.split("_")[1]
    var post = "";
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            $("#alertMsg").html(alertMsg);
            $("#responseMsg").html($.trim(this.responseText));
            $("#actionAlert").addClass("active");
            $("#actionAlert").slideDown(500, 'easeInOutQuint', function(){
                $("#actionAlert").delay(2500).slideUp(500, 'easeInOutQuint');
            });
            updateState();
        }
    };
    switch (room) {
        case "mb":
            post = "../includes/masterbed-inc.php";
            break;
        case "lr":
            post = "../includes/livingroom-inc.php"
            break;
        case "foyer":
            post = "../includes/foyer-inc.php"
            break;
        default:
            break;
    }
    xhttp.open("POST", post, true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    switch (action) {
        case "light-on-off":
            alertMsg = "The light on-off button was pressed.";
            xhttp.send("light=on-off");
            break;
        case "light-dim":
            alertMsg = "The light dim button was pressed.";
            xhttp.send("light=dim");
            break;
        case "fan-off":
            alertMsg = "The fan off button was pressed.";
            xhttp.send("fan=off");
            break;
        case "fan-low":
            alertMsg = "The fan low button was pressed.";
            xhttp.send("fan=low");
            break;
        case "fan-med":
            alertMsg = "The fan medium button was pressed.";
            xhttp.send("fan=medium");
            break;
        case "fan-high":
            alertMsg = "The fan high button was pressed.";
            xhttp.send("fan=high");
            break;
        case "hb-mode":
            alertMsg = "The heartbeat button was pressed.";
            xhttp.send("hbMode=on-off");
            break;
        case "tv-mode":
            alertMsg = "The tv mode on-off button was pressed.";
            xhttp.send("tvMode=on-off");
            break;
        case "night-time":
            alertMsg = "Night mode engaged.";
            xhttp.send("night=on-off");
            break;
        default:
            alertMsg = "There was an error.";
            break;
    }
}

$("#closeAlert").click(function() {
    $("#actionAlert").clearQueue();
    $("#actionAlert").slideUp(500, 'easeInOutQuint');
    });