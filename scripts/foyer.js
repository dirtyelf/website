$(document).ready(updateState);

function updateState() {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            //set the color of the lightbulb appropriately
            if(this.responseText == "on") {
                $("#lightBulb").css("color", "#fbff0f")
            } else {
                $("#lightBulb").css("color", "#737373")
            }
        }
    };
    xhttp.open("POST", "../includes/foyer-inc.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send("light=request");
}

function callSwitch(btnID) {
    var alertMsg = "";
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            $("#alertMsg").html(alertMsg);
            $("#responseMsg").html($.trim(this.responseText));
            $("#actionAlert").addClass("active");
            $("#actionAlert").slideDown(500, 'easeInOutQuint', function(){
                $("#actionAlert").delay(2500).slideUp(500, 'easeInOutQuint');
            });
            updateState();
        }
    };
    xhttp.open("POST", "../includes/foyer-inc.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    switch (btnID) {
        case "light-on-off":
            alertMsg = "The light on-off button was pressed.";
            xhttp.send("light=on-off");
            break;
        default:
            alertMsg = "There was an error.";
            break;
    }
}

$("#closeAlert").click(function() {
    $("#actionAlert").clearQueue();
    $("#actionAlert").slideUp(500, 'easeInOutQuint');
    });