<?php
session_start();

require 'includes/db-inc.php';

$loggedIn = false;
$cardCount = 0;

if (isset($_SESSION['user_id'])) {
    include 'includes/pageVisit-inc.php';
    $uid = $_SESSION['user_id'];
    $ufirst = $_SESSION['user_first'];
}

if (isset($_SESSION['user_active'])) {
    if($_SESSION['user_active'] == 1) {
        $loggedIn = true;
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Guestbook</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="styles/style.css">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <!-- Dismissible Post Alerts -->
    <?php if (isset($_GET['posting']) && $_GET['posting']=="success"): ?>
    <div class="alert alert-success alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Success!</strong> Thank you for posting!
    </div>
    <?php elseif (isset($_GET['posting']) && $_GET['posting']=="fail"): ?>
    <div class="alert alert-warning alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oops!</strong> Something went wrong. If this error continues contact the webmaster.
    </div>
    <?php elseif (isset($_GET['posting']) && $_GET['posting']=="empty"): ?>
    <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Oops!</strong> Please enter a message.
    </div>
    <?php else: ?>
    <!-- No Alert -->
    <?php endif; ?>

    <!-- Main Guestbook Display -->
    <div class="container my-sm-3">
        <h3>Lindsay and Bryan's
            <small class="text-muted">Guestbook</small>
        </h3>
        <!--<h1>Lindsay and Bryan's Guestbook</h1>-->
        <?php if($loggedIn && $_SESSION['user_active']): ?>
        <div id="submitPost" class="container">
            <form action="includes/guestbook-inc.php" method="post">
                <div class="form-group">
                    <label for="comment">Hello
                        <?php echo $ufirst ?>, enter your post:</label>
                    <textarea class="form-control" rows="3" name="postContent"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary" name="submitPost">Submit</button>
                </div>
            </form>
        </div>
        <?php else: ?>
        <div id="submitPost" class="container">
            <form action="#" method="post">
                <div class="form-group">
                    <?php if(!isset($_SESSION['user_active'])): ?>
                    <label for="comment">Login or <a href="signup.php">register</a> to add your comment:</label>
                    <?php elseif($_SESSION['user_active'] == '0'): ?>
                    <label for="comment">Please activate your account to add your post, check your email.</label>
                    <?php else: ?>
                    <label for="comment">Login or <a href="signup.php">register</a> to add your comment:</label>
                    <?php endif; ?>
                    <textarea class="form-control" rows="2" name="postContent" disabled></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary" name="edit" disabled>Submit</button>
                </div>
            </form>
        </div>
        <?php endif; ?>
        <h3><small class="text-muted">Guestbook Entries</small></h3>
        <div class="container" id="entries">
            <div class="card-deck">
                <?php 
    $sql = "SELECT u.user_uname, u.user_first, g.post_date, g.post_content, g.likes, g.post_id ";
    $sql .= "FROM users AS u, guestbook AS g ";
    $sql .= "WHERE u.user_id=g.user_id AND g.post_type='standard' ";
    $sql .= "ORDER BY g.post_date ASC;";
    $result = mysqli_query($connection, $sql);
    $numEntries = mysqli_num_rows($result);
    while ($row = mysqli_fetch_assoc($result)): ?>
                <div class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h3 class="my-0">
                            <?php echo htmlspecialchars($row['user_uname']).' <small class="text-muted">('.htmlspecialchars($row['user_first']).')</small>'; ?>
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo htmlspecialchars($row['post_content']); ?>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="btn-group btn-group-sm">
                                <?php if($loggedIn && $_SESSION['user_active']): ?>
                                <button type="button" class="btn btn-outline-primary" onclick="likePost(<?php echo htmlspecialchars($row['post_id']); ?>)">
                                    <?php else: ?>
                                    <button type="button" class="btn btn-outline-primary" onclick="alert('Login or Register to like posts!')" disabled>
                                        <?php endif; ?>
                                        Like (<span id="numLikes"><?php echo htmlspecialchars($row['likes']); ?></span>)
                                    </button>
                                    <?php if($loggedIn && $_SESSION['user_active']): ?>
                                    <button type="button" class="btn btn-outline-primary" onclick="alert('Reply! Logged in.')">
                                        <?php else: ?>
                                        <button type="button" class="btn btn-outline-primary" onclick="alert('Reply!')" disabled>
                                            <?php endif; ?>
                                            Reply
                                        </button>
                            </div>
                        </li>
                    </ul>
                    <div class="card-footer">
                        <p class="my-0"><small class="text-muted">
                                <?php echo htmlspecialchars($row['post_date']); ?>
                            </small></p>
                    </div>
                </div>
                <?php 
$cardCount += 1;
if ($cardCount % 2 == 0): ?>
                <div class="w-100 d-none d-sm-block d-md-none">
                    <!-- wrap every 2 on sm-->
                </div>
                <?php endif; ?>
                <?php if ($cardCount % 3 == 0): ?>
                <div class="w-100 d-none d-md-block d-lg-none">
                    <!-- wrap every 3 on md-->
                </div>
                <?php endif; ?>
                <?php if ($cardCount % 4 == 0): ?>
                <div class="w-100 d-none d-lg-block d-xl-none">
                    <!-- wrap every 4 on lg-->
                </div>
                <?php endif; ?>
                <?php if ($cardCount % 5 == 0): ?>
                <div class="w-100 d-none d-xl-block">
                    <!-- wrap every 5 on xl-->
                </div>
                <?php endif; ?>

                <?php /* if($_SESSION('user_role') == 'admin'): ?>
                <br>
                <p class="nameGB">
                    <?php echo htmlspecialchars($row['user_uname']).' ('.htmlspecialchars($row['user_first']).')'; ?>
                </p>
                <p class="dateGB">
                    <?php echo htmlspecialchars($row['post_date']); ?>
                </p>
                <p class="postGB">
                    <?php echo htmlspecialchars($row['post_content']); ?>
                </p>
                <?php endif; */ ?>
                <?php endwhile; ?>
            </div>
        </div>

        <!-- Include the Footer Jumbotron -->
        <?php include 'common/jumbotronbot.php'; ?>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
        <script src="/scripts/guestbook.js"></script>

</body>

</html>