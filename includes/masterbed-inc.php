<?php
session_start();

if (!isset($_POST)) {
    header("Location: index.php");
}

$lightStatus = $fanStatus = $hbMode = $response = $nightMode = '';

if (isset($_POST)) {

    // check to see if the light is changing
    if(isset($_POST["light"])) {
    	$lightStatus = $_POST["light"];
    } else {
    	$lightStatus = "NONE SET";
    }

    if($lightStatus == "on-off"){
    	$curl = curl_init('http://192.168.1.176/light');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif($lightStatus == "dim"){
    	$curl = curl_init('http://192.168.1.176/dim');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    }

    // check to see if the fan is changing
    if(isset($_POST["fan"])) {
    	$fanStatus = $_POST["fan"];
    } else {
    	$fanStatus = "NONE SET";
    }

    if($fanStatus == "high"){
    	$curl = curl_init('http://192.168.1.176/fanHi');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif($fanStatus == "medium"){
    	$curl = curl_init('http://192.168.1.176/fanMed');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif($fanStatus == "low"){
    	$curl = curl_init('http://192.168.1.176/fanLow');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    } elseif($fanStatus == "off"){
    	$curl = curl_init('http://192.168.1.176/fanOff');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    }

    // if tvMode is set
    if(isset($_POST["hbMode"])) {
    	$curl = curl_init('http://192.168.1.176/hbToggle');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    }

    // nightmode?
    if(isset($_POST["night"])) {
    	$curl = curl_init('http://192.168.1.176/night');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    }
}

if ($response != '') {
    echo($response);
} else {
    $response = 'Error. No action was set.';
    echo($response);
}

?>