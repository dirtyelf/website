<?php

require 'db-inc.php';
session_start();

if (!isset($_POST['forgot'])) {

    header("Location: ../index.php");
    exit();

} else {
    $uname = mysqli_real_escape_string($connection, $_POST['uname']);
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $sql = "SELECT * FROM users WHERE user_email='$email' AND user_uname='$uname'";
    $result = mysqli_query($connection, $sql);
    $resultCheck = mysqli_num_rows($result);
    
    if ($resultCheck == 0) {

        header("Location: ../forgot.php?reset=notFound");
        exit();
        
    } else {
        $row = mysqli_fetch_assoc($result);
        $firstName = $row['user_first'];
        $userName = $row['user_uname'];
        $email = $row['user_email'];
        $unique_hash = $row['user_hash'];

        // send a reset email
        $to = $email;
        $headers = 'From: "no reply" <no-reply@lindsayandbryan.com>';
        $subject = 'Reset your password at LindsayandBryan.com';
        $msg = '
        Hello ' . $firstName . ', 
        
        You have requested to reset your password for the user name: ' . $userName . '.
        
        Click this link to reset your password.
        
        https://www.lindsayandbryan.com/reset.php?email=' . $email . '&hash=' . $unique_hash . '

        Thanks,
        
        B-ry
        
        *please do not reply to this email, the inbox is not monitored';

        mail($to, $subject, $msg, $headers);

        header("Location: ../forgot.php?reset=linkSent");
        exit();
    }
}

?>