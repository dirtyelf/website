<?php

session_start();

if (!isset($_POST['login']) && !isset($_POST['forgot'])) {
    
    header("Location: ../index.php");
    exit();

} elseif (isset($_POST['forgot'])) {

    header("Location: ../forgot.php");
    exit();

} else {

    include 'db-inc.php';

    $userID = mysqli_real_escape_string($connection, $_POST['userName']);
    $userPW = mysqli_real_escape_string($connection, $_POST['password']);

    // error checking
    // empty checking
    if (empty($userID) || empty($userPW)) {
        header("Location: ../index.php?login=empty");
        exit();
    } else {
        $sql = "SELECT * FROM users WHERE user_uname='$userID' OR user_email='$userID'";
        $result = mysqli_query($connection, $sql);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck < 1) {
            header("Location: ../index.php?login=notFound");
            exit();
        } else {
            if ($row = mysqli_fetch_assoc($result)) {
                // pw hash check
                $hashedPwdCheck = password_verify($userPW, $row['user_pwd']);
                if ($hashedPwdCheck == false) {
                    mysqli_free_result($result);
                    header("Location: ../index.php?login=fail");
                    exit();
                } elseif ($hashedPwdCheck == true) {
                    // log in the user
                    $_SESSION['user_id'] = $row['user_id'];
                    $_SESSION['user_first'] = $row['user_first'];
                    $_SESSION['user_last'] = $row['user_last'];
                    $_SESSION['user_uname'] = $row['user_uname'];
                    $_SESSION['user_email'] = $row['user_email'];
                    $_SESSION['user_active'] = $row['user_active'];
                    $_SESSION['user_role'] = $row['user_role'];
                    $_SESSION['user_hash'] = $row['user_hash'];
                    $_SESSION['login_page'] = $url = '';
                    mysqli_free_result($result);
                    $sql = "SELECT lastPage from last_page WHERE user_id=";
                    $sql .= $_SESSION['user_id'];
                    $result = mysqli_query($connection, $sql);
                    if(mysqli_num_rows($result) == 0){
                        header("Location: ../index.php?login=success");
                        exit();
                    }else{
                        $row = mysqli_fetch_assoc($result);
                        header("Location: " . $row['lastPage']);
                    }
                }
            }
        }
    }
}

?>