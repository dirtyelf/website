<?php

session_start();
require_once 'db-inc.php';
require_once 'recaptchalib.php';

if (!isset($_POST['submit'])) {
    header("Location: ../signup.php");
}

$secretKey = "6Lf6O3MUAAAAANKD8mzvlXUow25GOWe4GLdrjoc7";
$response = null;
$reCaptcha = new ReCaptcha($secretKey);

if (isset($_POST["g-recaptcha-response"])) {
    $response = $reCaptcha->verifyResponse($_SERVER["REMOTE_ADDR"], $_POST["g-recaptcha-response"]);
}

if ($response != null && $response->success) {
    // clean the inputs
    $cleaned_first = mysqli_real_escape_string($connection, $_POST['firstName']);
    $cleaned_last = mysqli_real_escape_string($connection, $_POST['lastName']);
    $cleaned_uname = mysqli_real_escape_string($connection, $_POST['userName']);
    $cleaned_email = mysqli_real_escape_string($connection, $_POST['email']);
    $cleaned_pwd = mysqli_real_escape_string($connection, $_POST['pwd']);
    $cleaned_pwdC = mysqli_real_escape_string($connection, $_POST['pwdC']);
    $unique_hash = mysqli_real_escape_string($connection, md5(rand(0,1000)));

    // error checking
    // check empty
    if (empty($cleaned_first) || empty($cleaned_last) || empty($cleaned_email) || empty($cleaned_uname) || empty($cleaned_pwd) || empty($cleaned_pwdC)) {
        header("Location: ../signup.php?signup=empty");
    } else {
        // check for valid name inputs
        if (!preg_match("/^[a-zA-Z]*$/", $cleaned_first) || !preg_match("/^[a-zA-Z]*$/", $cleaned_last)) {
            header("Location: ../signup.php?signup=invalidName");
        } else {
            // check to see if email already exists
            $sql = "SELECT * FROM users WHERE user_email='$cleaned_email'";
            $result = mysqli_query($connection, $sql);
            $resultCheck = mysqli_num_rows($result);
            if ($resultCheck > 0) {
                header("Location: ../signup.php?signup=emailExists");
            } else {
                // check if username exists
                $sql = "SELECT * FROM users WHERE user_uname='$cleaned_uname'";
                $result = mysqli_query($connection, $sql);
                $resultCheck = mysqli_num_rows($result);
                if ($resultCheck > 0) {
                    header("Location: ../signup.php?signup=usernameExists");
                } else {
                    // check for pw requiremnts
                    if ($cleaned_pwd <> $cleaned_pwdC) {
                        header("Location: ../signup.php?signup=pwdConfirm");
                    } else {
                        // hash pw
                        $hashedPwd = password_hash($cleaned_pwd, PASSWORD_DEFAULT);
                        
                        // create user entry in DB
                        $sql = "INSERT INTO users (user_first, user_last, user_uname, user_email, user_pwd, user_hash) VALUES ('$cleaned_first', '$cleaned_last', '$cleaned_uname', '$cleaned_email', '$hashedPwd', '$unique_hash');";
                        $result = mysqli_query($connection, $sql);
                        if (!$result) {
                            header("Location: ../signup.php?signup=dbFail");
                        }

                        // create entry in last page table
                        $uID = mysqli_insert_id($connection);
                        $sql = "INSERT INTO last_page VALUES ($uID, '/', NULL, NULL)";
                        $result = mysqli_query($connection, $sql);
                        if (!$result) {
                            header("Location: ../signup.php?signup=lastFail");
                        }

                        // send a registration email
                        $to = $cleaned_email;
                        $headers = 'From: no-reply@lindsayandbryan.com';
                        $subject = 'Please verify your account at LindsayandBryan.com';
                        $msg = 
'Hello, ' . $cleaned_first . '! (or do you prefer ' . $cleaned_uname . '?)

Thank you for registering!

Click this link to complete your registration and activate your account.

https://www.lindsayandbryan.com/verify.php?email=' . $cleaned_email . '&hash=' . $unique_hash . '

Thanks,

B-ry

*please do not reply to this email, the inbox is not monitored';

                        $msg = wordwrap($msg, 70);
                        mail($to, $subject, $msg, $headers);
                        if (session_status() == PHP_SESSION_ACTIVE) {
                            session_destroy();
                        }
                        header("Location: ../index.php?signup=success");
                    }
                }
            }
        }
    }
} else {
    header("Location: ../signup.php?signup=reCaptcha");
}