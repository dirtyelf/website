<?php

session_start();

require 'db-inc.php';

if (isset($_GET['hash'])){
    $hash = mysqli_real_escape_string($connection, $_GET['hash']);
    $sql = "SELECT user_role FROM users WHERE user_hash='$hash'";
    $result = mysqli_query($connection, $sql);
    $row = mysqli_fetch_assoc($result);
    $_SESSION['user_role'] = 'not provided by session';
} 
else {
    $row['user_role'] = 'not provided by hash';
}

// Check admin session.
if ($_SESSION['user_role'] == 'admin' || $row['user_role'] == 'admin'){
    
    // ADMIN GET Methods
    if ($_SERVER['REQUEST_METHOD'] == 'GET'){
        
        if (isset($_GET['table']) && isset($_GET['id'])){
            echo get_specific_id_by_table($_GET['table'], $_GET['id'], $connection);
            return;
        }

        if (isset($_GET['table']) && isset($_GET['col'])){
            if (isset($_GET['sort'])){
                echo get_table_col($_GET['table'], $_GET['col'], $_GET['sort'], $connection);
            } else {
                echo get_table_col($_GET['table'], $_GET['col'], false, $connection);
            }
            return;
        }
        
        if (isset($_GET['entryForm']) && isset($_GET['id'])){
            echo get_table_entry_form($_GET['entryForm'], $_GET['id'], $connection);
            return;
        }

        if (isset($_GET['entryForm']) && isset($_GET['notdba'])){
            echo get_table_entry_form($_GET['entryForm'], 'notdba', $connection);
            return;
        }
        
        if (isset($_GET['table'])) {
            echo get_table($_GET['table'], $connection);
            return;
        }

        if (isset($_GET['entryForm'])){
            echo get_table_entry_form($_GET['entryForm'], 'newRecord', $connection);
            return;
        }
        
        echo 'No getter for that resource.';
        return;
    }
    
    // ADMIN POST Methods (add records)
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $decodedJSON = json_decode(trim(stripslashes(file_get_contents("php://input")),'"'), true);
        $tbl = $decodedJSON['table'];
        $dataArr = $decodedJSON['data'];
        echo add_record_to_table($tbl, $dataArr, $connection);
        return;
    }
    
    // ADMIN PUT Methods (edit records)
    if ($_SERVER['REQUEST_METHOD'] == 'PUT'){
        $decodedJSON = json_decode(trim(stripslashes(file_get_contents("php://input")),'"'), true);
        $tbl = $decodedJSON['table'];
        $rowID = $decodedJSON['id'];
        $priKey = $decodedJSON['key'];
        $dataArr = $decodedJSON['data'];
        echo edit_table($tbl, $rowID, $priKey, $dataArr, $connection);
        return;
    }
    
    // ADMIN DELETE Methods (delete records)
    if ($_SERVER['REQUEST_METHOD'] == 'DELETE'){
        $decodedJSON = json_decode(trim(stripslashes(file_get_contents("php://input")),'"'), true);
        $tbl = $decodedJSON['table'];
        $rowID = $decodedJSON['id'];
        if (delete_row($tbl, $rowID, $connection)){
            echo 'Deleted id ' . $rowID . ' from ' . $tbl . '.';
            return;
        } else {
            echo 'Nothing was deleted.';
            return;
        }
    } 
    
} 
elseif ($_SESSION['user_role'] == 'resident' || $row['user_role'] == 'resident'){

    // RESIDENT GET Methods
    if ($_SERVER['REQUEST_METHOD'] == 'GET'){

        if (isset($_GET['table']) && isset($_GET['col'])){
            if (isset($_GET['sort'])){
                echo get_table_col($_GET['table'], $_GET['col'], $_GET['sort'], $connection);
            } else {
                echo get_table_col($_GET['table'], $_GET['col'], false, $connection);
            }
            return;
        }

        if (isset($_GET['entryForm']) && isset($_GET['notdba'])){
            echo get_table_entry_form($_GET['entryForm'], 'notdba', $connection);
            return;
        }
        
        echo 'No getter for that resource.';
        return;
    }
    
    // POST Methods (add records)
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $decodedJSON = json_decode(trim(stripslashes(file_get_contents("php://input")),'"'), true);
        $tbl = $decodedJSON['table'];
        $dataArr = $decodedJSON['data'];
        echo add_record_to_table($tbl, $dataArr, $connection);
        return;
    }

    echo 'Resident functions for that verb or function not implemented.';
    return;
} 
else {
    // return a 403?
    echo('Not Authorized. ');
    return;
}

function get_table($tbl_name, $conn){
    // returns html/bootstrap formatted database table
    $cleaned_tbl = mysqli_real_escape_string($conn, $tbl_name);
    $sql = "SHOW TABLES LIKE '" . $cleaned_tbl . "'";
    $res = mysqli_query($conn, $sql);
    $tblExists = mysqli_num_rows($res);
    if ($tblExists !== 1){
        return 'Table "' . htmlspecialchars($tbl_name) . '" does not exist.';
    }
    $tblOutput = "";
    $sql = "SHOW columns FROM " . $cleaned_tbl;
    $resultCols = mysqli_query($conn, $sql);
    $sql = "SELECT * FROM " . $cleaned_tbl;
    $resultData = mysqli_query($conn, $sql);
    if (!$resultCols){
        $tblOutput = 'Database Error. (cols)';
    } 
    elseif (!$resultData){
        $tblOutput = 'Database Error. (data)';
    } 
    else {
        $tblOutput = '<div class="table-responsive-sm my-3">';
        $tblOutput .= '<table class="table table-striped table-hover">';
        $tblOutput .= '<thead>';
        $tblOutput .= '<tr>';
        while ($row = mysqli_fetch_assoc($resultCols)) {
            if ($row['Key'] == 'PRI'){
                $primaryKeyCol = $row['Field'];
                $tblOutput .= '<th>' . $row['Field'] . ' (PRI)</th>';
            }
            else {
                $tblOutput .= '<th>' . $row['Field'] . '</th>';
            }
        }
        $tblOutput .= '<th>Actions</th>';
        $tblOutput .= '</tr>';
        $tblOutput .= '</thead>';
        $tblOutput .= '<tbody>';
        mysqli_free_result($resultCols);
        while ($row = mysqli_fetch_assoc($resultData)) {
            $primaryKeyColValue = $row[$primaryKeyCol];
            $tblOutput .= '<tr>';
            foreach ($row as $key => $value) {
                if ($key == 'user_pwd' || $key == 'user_hash'){
                    $tblOutput .= '<td>*</td>';
                }
                else {
                    $tblOutput .= '<td>' . $value . '</td>';
                }
            }
            $tblOutput .= '<td>';
            $tblOutput .= '<button class="btn btn-secondary mx-1 editRow" data-toggle="modal" data-target="#entryModal" data-id="';
            $tblOutput .= $primaryKeyColValue;
            $tblOutput .= '">Edit</button>';
            $tblOutput .= '<button class="btn btn-danger mx-1 deleteRow" id="';
            $tblOutput .= $primaryKeyColValue;
            $tblOutput .= '">Delete</button>';
            $tblOutput .= '</td>';
            $tblOutput .= '</tr>';
        }
        mysqli_free_result($resultData);
        $tblOutput .= '</tbody>';
        $tblOutput .= '</table>';
        $tblOutput .= '</div>';
    }
    return $tblOutput;
}

function get_table_entry_form($table, $id, $conn){
    // returns html/bootstrap formatted form with fields for each column
    $newRecord = false;
    $emptyTable = false;
    if ($id == 'newRecord' || $id == 'notdba'){
        $newRecord = true;
    }
    $formOutput = "";
    $table_id = mysqli_real_escape_string($conn, $table);
    $sql = "SHOW columns from " . $table_id . " WHERE `Key`='PRI'";
    $res = mysqli_query($conn, $sql);
    if (!$res){
        $formOutput = "Error Retrieving Form (prikey)";
        return $formOutput;
    }
    $res_row = mysqli_fetch_assoc($res);
    $priKey = $res_row['Field'];
    if ($res_row['Extra'] == 'auto_increment'){
        $autoInc = true;
    }
    else{
        $autoInc = false;
    }
    if ($newRecord){
        $sql = "SELECT * FROM $table_id LIMIT 1";
        $res = mysqli_query($conn, $sql);
        if (!$res || mysqli_affected_rows($conn) != 1){
            $emptyTable = true;
            $sql = "SELECT column_name FROM information_schema.columns WHERE table_name='$table_id'";
            $res = mysqli_query($conn, $sql);
            if (!$res){
                $formOutput = "Error Retrieving Form (data-noid)";
                return $formOutput;
            }
        }
    }
    else {
        $row_id = mysqli_real_escape_string($conn, $id);
        $sql = "SELECT * FROM $table_id WHERE $priKey=$row_id";
        $res = mysqli_query($conn, $sql);
        if (!$res){
            $formOutput = "Error Retrieving Form (data-id)<br>";
            $formOutput .= $priKey . '<br>';
            $formOutput .= $row_id;
            return $formOutput;
        }
        if(mysqli_num_rows($res) != 1){
            $formOutput = "Duplicate primary keys. (data-key)";
            return $formOutput;
        }
    }
    while ($row = mysqli_fetch_assoc($res)){
        foreach ($row as $key => $value){
            $formOutput .= '<div class="form-group">';
            $formOutput .= '<div class="input-group input-group-sm">';
            $formOutput .= '<div class="input-group-prepend">';
            $formOutput .= '<span class="input-group-text">' . (($emptyTable) ? $value : $key) .'</span>';
            $formOutput .= '</div>';
            if(!$newRecord){
                if ($key == $priKey){
                    $formOutput .= '<input type="text" class="form-control form-control-sm" id="' . $key . '" value="' . $value . '" disabled>';
                }
                else {
                    $formOutput .= '<input type="text" class="form-control form-control-sm" id="' . $key . '" value="' . $value . '">';
                }
            }
            else {
                if ((($emptyTable) ? $value : $key) == $priKey){
                    if ($autoInc){
                        $formOutput .= '<input type="text" class="form-control form-control-sm" id="' . (($emptyTable) ? $value : $key) . '" value="[auto-increment]" disabled>';
                    }
                    else {
                        $formOutput .= '<input type="text" class="form-control form-control-sm" id="' . (($emptyTable) ? $value : $key) . '">';
                    }
                }
                else {
                    $formOutput .= '<input type="text" class="form-control form-control-sm" id="' . (($emptyTable) ? $value : $key) . '">';
                }
            }
            $formOutput .= '</div>';
            $formOutput .= '</div>';
        }
    }
    if ($table == 'makenzie' && $id == 'notdba'){
        $formOutput .= '<button type="button" class="btn btn-primary" id="makenzieSave">Save to DB</button>';
    }
    return $formOutput;
}

function get_specific_id_by_table($table, $id, $conn){
    return 'NOT IMPLEMENTED';
}

function get_table_row_by_pri_key($table, $row, $conn){
    
    return 'NOT IMPLEMENTED';
    
    $cleaned_tbl = mysqli_real_escape_string($conn, $table);
    $cleaned_row = mysqli_real_escape_string($conn, $row);
    $sql = "SHOW columns from " . $cleaned_tbl . "WHERE `Key`='PRI'";
    $res_row = mysqli_fetch_assoc(mysqli_query($conn, $sql));
    $priKey = $res_row['Field'];
    $sql = "SELECT * FROM " . $cleaned_tbl . "WHERE " . $priKey . "=" . $cleaned_row;
    $res = mysqli_query($conn, $sql);
    //if blah blah blah does exist... return it
}

function get_table_col($table, $col, $sort, $conn){
    $cleaned_tbl = mysqli_real_escape_string($conn, $table);
    $cleaned_col = mysqli_real_escape_string($conn, $col);
    if(!$sort){
        $sql = "SELECT " . $cleaned_col . " from " . $cleaned_tbl;
    } else {
        $sql = "SELECT " . $cleaned_col . " from " . $cleaned_tbl . " order by " . $sort . " asc";
    }
    
    $res = mysqli_query($conn, $sql);
    if (!$res){
        return 'Column ' . htmlspecialchars($col) . ' does not exist in table ' . htmlspecialchars($table) . '.';
    }
    return json_encode(mysqli_fetch_all($res));
}

function add_record_to_table($tbl, $data, $conn){
    $returnMsg = '';
    $colKeys = '';
    $colVals = '';
    $cleaned_tbl = mysqli_real_escape_string($conn, $tbl);
    $sql = "SHOW columns from " . $cleaned_tbl . " WHERE `Key`='PRI'";
    $res = mysqli_query($conn, $sql);
    if (!$res){
        $returnMsg = "Error Adding Record (prikey)";
        return $returnMsg;
    }
    $res_row = mysqli_fetch_assoc($res);
    $priKey = $res_row['Field'];
    if ($res_row['Extra'] == 'auto_increment'){
        $needsPriKey = false;
    }
    else {
        $needsPriKey = true;
    }
    $sql = 'INSERT INTO ' . $tbl . ' (';
    foreach ($data as $edit) {
        $keys = array_keys($edit);
        $tblKey = mysqli_real_escape_string($conn, $keys[0]);
        $tblVal = mysqli_real_escape_string($conn, $edit[$keys[0]]);
        if ($tblKey == $priKey && !$needsPriKey) { continue; }
        $colKeys .= $tblKey . ', ';
        if ($tblVal == 'NULL' || is_numeric($tblVal)){
            $colVals .= $tblVal . ', ';
        }
        else {
            $colVals .= "'" . $tblVal . "', ";
        }
    }
    $colKeys = rtrim($colKeys, ", ");
    $colVals = rtrim($colVals, ", ");
    $sql .= $colKeys . ') VALUES (' . $colVals . ')';
    $result = mysqli_query($conn, $sql);
    $numUpdated = mysqli_affected_rows($conn);
    if ($numUpdated == 0) {
        $returnMsg = 'POST request failed. No data altered.';
        return $returnMsg;
    }
    elseif ($numUpdated > 1) {
        $returnMsg = 'More than one row updated. Database changed, probably not for the better...';
        return $returnMsg;
    }
    elseif ($numUpdated == 1) {
        $returnMsg = 'POST request Success. New record added to database.';
        return $returnMsg;
    }
    else {
        $returnMsg = 'ERROR. POST request failed.<br>';
        return $returnMsg;
    }
}

function edit_table($tbl, $id, $key, $data, $conn){
    $returnMsg = '';
    $sql ='';
    $cleaned_id = mysqli_real_escape_string($conn, $id);
    $cleaned_tbl = mysqli_real_escape_string($conn, $tbl);
    $cleaned_key = mysqli_real_escape_string($conn, $key);
    $sql .= 'UPDATE ' . $tbl . ' SET ';
    foreach ($data as $edit) {
        $keys = array_keys($edit);
        $tblKey = mysqli_real_escape_string($conn, $keys[0]);
        $tblVal = mysqli_real_escape_string($conn, $edit[$keys[0]]);
        if ($tblVal == 'NULL'){
            $sql .= $tblKey . "=" . $tblVal . ", ";
        }
        else {
            $sql .= $tblKey . "=" . "'" . $tblVal . "', ";
        }
    }
    $sql = rtrim($sql, ", ");
    $sql .= ' WHERE ' . $cleaned_key . '=' . $cleaned_id;
    $result = mysqli_query($conn, $sql);
    $numUpdated = mysqli_affected_rows($conn);
    if ($numUpdated == 0) {
        $returnMsg = 'PUT request failed. No data altered.';
        return $returnMsg;
    }
    elseif ($numUpdated > 1) {
        $returnMsg = 'More than one row updated. Database changed, probably not for the better...';
        return $returnMsg;
    }
    elseif ($numUpdated == 1) {
        $returnMsg = 'PUT request Success. Changes saved in database.';
        return $returnMsg;
    }
    else {
        $returnMsg = 'ERROR. PUT request failed.';
        return $returnMsg;
    }
}

function delete_row($table, $row, $conn){
    // Deletes a row from a table by Primary_key ID.
    $table_id = mysqli_real_escape_string($conn, $table);
    $row_id = mysqli_real_escape_string($conn, $row);
    $sql = "SHOW KEYS FROM $table_id WHERE Key_name = 'PRIMARY'";
    $res = mysqli_query($conn, $sql);
    if (!$res) {
        return FALSE;
    } 
    $res_row = mysqli_fetch_assoc($res);
    $row_key = $res_row['Column_name'];
    $sql = "DELETE FROM $table_id WHERE $row_key='$row_id'";
    $res = mysqli_query($conn, $sql);
    if (mysqli_affected_rows($conn) > 0){
        return TRUE;
    } 
    else {
        return FALSE;
    }
}