<?php
session_start();

require 'db-inc.php';

if (!isset($_POST)){
    header("Location: ../index.php");
}

if (isset($_POST['l'])) {
    $cleanedPostID = mysqli_real_escape_string($connection, $_POST['l']);
    $uid = $_SESSION['user_id'];
    $sql = "SELECT likes FROM guestbook WHERE post_id='$cleanedPostID'";
    $numLikes = intval(mysqli_fetch_assoc(mysqli_query($connection, $sql))['likes']) + 1;
    $sql = "SELECT * FROM user_likes WHERE user_id='$uid' AND post_id='$cleanedPostID'";
    $result = mysqli_query($connection, $sql);
    if (mysqli_num_rows($result) == 0) {
        // add liked posted to user_likes table
        $sql = "INSERT IGNORE INTO user_likes (user_id, post_id) ";
        $sql .= "VALUES ($uid, $cleanedPostID);";
        $result = mysqli_query($connection, $sql);
        // set new number of likes in post table
        $sql = "UPDATE guestbook SET likes='$numLikes' WHERE post_id='$cleanedPostID'";
        $result = mysqli_query($connection, $sql);
        echo($numLikes);
        exit();
    }
    echo($numLikes - 1);
    exit();
}

if (isset($_POST['postContent']) && $_POST['postContent'] == '') {
    header("Location: ../guestbook.php?posting=empty");
}
elseif (isset($_POST['postContent']) && !empty($_POST['postContent'])) {
    $cleanedPost = mysqli_real_escape_string($connection, $_POST['postContent']);
    //user id, post date, post content, post type, post level
    $uid = $_SESSION['user_id'];
    $sql = "INSERT INTO guestbook (user_id, post_date, post_content, post_type, post_level) ";
    $sql .= "VALUES ($uid, NOW(), '$cleanedPost', 'standard', 0);";
    $result = mysqli_query($connection, $sql);
    if (!$result) {
        header("location: ../guestbook.php?posting=fail");    
    } else {
        header("location: ../guestbook.php?posting=success");
    }
} else {
    header("location: ../guestbook.php?posting=fail");
}