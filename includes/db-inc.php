<?php

$cred = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../db');
$creds = explode(' ',$cred);

$dbServerName = trim($creds[0]);
$dbUserName = trim($creds[1]);
$dbPassword = trim($creds[2]);
$dbName = trim($creds[3]);

$connection = mysqli_connect($dbServerName, $dbUserName, $dbPassword, $dbName) or die(mysqli_error($connection));