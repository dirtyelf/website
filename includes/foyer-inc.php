<?php
session_start();

if (!isset($_POST)) {
    header("Location: index.php");
}

$lightStatus = $fanStatus = $tvMode = $response = '';

if (isset($_POST)) {

    // check to see if the light is changing
    if(isset($_POST["light"])) {
    	$lightStatus = $_POST["light"];
    } else {
    	$lightStatus = "NONE SET";
    }

    if($lightStatus == "on-off"){
    	$curl = curl_init('http://192.168.1.177/relay');
    	curl_setopt($curl, CURLOPT_PORT, 18008);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    }

    if($lightStatus == "request"){
        $curl = curl_init('http://192.168.1.177/state');
        curl_setopt($curl, CURLOPT_PORT, 18008);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
        $response = curl_exec($curl);
        curl_close($curl);
    }
}

if ($response != '') {
    echo($response);
} else {
    $response = 'Error. No action was set.';
    echo($response);
}

?>