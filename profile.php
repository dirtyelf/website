<?php
session_start();
if (!isset($_SESSION['user_id']) || !$_SESSION['user_active']) {
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>
        <?php echo(htmlspecialchars($_SESSION['user_uname'] . "'s Profile")); ?>
    </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">

</head>

<body>

    <!-- Navbar based on User Status -->
    <?php include 'common/navbar.php'; ?>

    <div class="container my-sm-3">
        <h1>User Profile</h1>
        <div class="row">
            <div class="col-sm-6">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item d-flex justify-content-center">
                        <span class="badge badge-dark mx-1">User ID</span>
                        <span class="badge badge-secondary mx-1">
                            <?php echo $_SESSION['user_id']; ?>
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-center">
                        <span class="badge badge-dark mx-1">User Name</span>
                        <span class="badge badge-secondary mx-1">
                            <?php echo(htmlspecialchars($_SESSION['user_uname'])); ?>
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-center">
                        <span class="badge badge-dark mx-1">First Name</span>
                        <span class="badge badge-secondary mx-1">
                            <?php echo(htmlspecialchars($_SESSION['user_first'])); ?>
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-center">
                        <span class="badge badge-dark mx-1">Last Name</span>
                        <span class="badge badge-secondary mx-1">
                            <?php echo(htmlspecialchars($_SESSION['user_last'])); ?>
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-center">
                        <span class="badge badge-dark mx-1">Email</span>
                        <span class="badge badge-secondary mx-1">
                            <?php echo(htmlspecialchars($_SESSION['user_email'])); ?>
                        </span>
                    </li>
                    <li class="list-group-item d-flex justify-content-center">
                        <span class="badge badge-dark mx-1">Role</span>
                        <span class="badge badge-secondary mx-1">
                            <?php echo(htmlspecialchars($_SESSION['user_role'])); ?>
                        </span>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <h1>Profile Picture</h1>
                <p>Profile picture and description go here</p>
                <p><?php echo $_SESSION['login_page'];?></p>
            </div>
        </div>
    </div>

    <!-- Include the Footer Jumbotron -->
    <?php include 'common/jumbotronbot.php'; ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>

</body>

</html>