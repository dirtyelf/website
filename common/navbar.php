<?php if (isset($_SESSION['user_id']) && $_SESSION['user_active'] && ($_SESSION['user_role'] == 'admin' || $_SESSION['user_role'] == 'resident')): ?>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
    <a class="navbar-brand" href="/index.php">the8008</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navCollapse">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="/index.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="/guestbook.php">Guestbook</a></li>
            <li class="nav-item"><a class="nav-link" href="/baby/index.php">Baby</a></li>
            <?php 
            $browser = get_browser(null, true);
            if($browser['ismobiledevice']): ?>
            <li class="nav-item"><a class="nav-link" href="/singleControl.php">Home Control</a></li>
            <?php else: ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Home Control</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../masterbedroom.php">Master</a>
                    <a class="dropdown-item" href="../livingroom.php">Living Room</a>
                    <a class="dropdown-item" href="../foyer.php">Foyer</a>
                    <a class="dropdown-item" href="#">Kitchen</a>
                    <a class="dropdown-item" href="#">Garage</a>
                </div>
                <?php endif; ?>
                <?php if ($_SESSION['user_role'] == 'admin'): ?>
            <li class="nav-item"><a href="/dba.php" class="nav-link">DB Admin</a></li>
            <?php endif; ?>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto pr-3">
            <?php if (isset($_GET['login']) && $_GET['login']=="success"): ?>
            <span class="navbar-text"></span>
            <?php else: ?>
            <span class="navbar-text"></span>
            <?php endif; ?>
        </ul>
    </div>

    <form class="form-inline float-sm-right" style="margin-bottom: 0;" action="/includes/logout-inc.php" method="post">
        <div class="input-group input-group-sm">
            <div class="input-group-append">
                <a href="/profile.php" class="btn btn-success rounded-left" role="button">
                    <?php echo(htmlspecialchars($_SESSION['user_uname'])); ?>
                </a>
                <button class="btn btn-primary" name="logout" type="submit">logout</button>
            </div>
        </div>
    </form>
</nav>

<?php elseif (isset($_SESSION['user_id']) && $_SESSION['user_active'] && !($_SESSION['user_role'] == 'admin' || $_SESSION['user_role'] == 'resident')): ?>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
    <a class="navbar-brand" href="#">the8008</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navCollapse">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="/index.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="/guestbook.php">Guestbook</a></li>
        </ul>
        <ul class="navbar-nav ml-auto pr-3">
            <?php if (isset($_GET['login']) && $_GET['login']=="success"): ?>
            <span class="navbar-text"></span>
            <?php else: ?>
            <span class="navbar-text"></span>
            <?php endif; ?>
        </ul>
    </div>

    <form class="form-inline float-sm-right" style="margin-bottom: 0;" action="/includes/logout-inc.php" method="post">
        <div class="input-group input-group-sm">
            <div class="input-group-append">
                <a href="/profile.php" class="btn btn-success rounded-left" role="button">
                    <?php echo(htmlspecialchars($_SESSION['user_uname']));?>
                </a>
                <button class="btn btn-primary" name="logout" type="submit">logout</button>
            </div>
        </div>
    </form>
</nav>

<?php elseif (isset($_SESSION['user_id']) && !$_SESSION['user_active']): ?>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
    <a class="navbar-brand" href="#">the8008</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navCollapse">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="/index.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="/guestbook.php">Guestbook</a></li>
        </ul>
        <ul class="navbar-nav ml-auto pr-3">
            <span class="navbar-text float-right">Please activate your account.</span>
        </ul>
    </div>

    <form class="form-inline float-sm-right" style="margin-bottom: 0;" action="/includes/logout-inc.php" method="post">
        <div class="input-group input-group-sm">
            <div class="input-group-append">
                <a href="/profile.php" class="btn btn-success rounded-left" role="button">
                    <?php echo(htmlspecialchars($_SESSION['user_uname']));?>
                </a>
                <button class="btn btn-primary" name="logout" type="submit">logout</button>
            </div>
        </div>
        </div>
    </form>
</nav>

<?php else: ?>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
    <a class="navbar-brand abs" href="#">the8008</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navCollapse">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="/index.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="/guestbook.php">Guestbook</a></li>
        </ul>

        <ul class="navbar-nav ml-auto pr-3">
            <?php if (isset($_GET['login']) && $_GET['login']=="notFound"): ?>
            <span class="navbar-text"></span>
            <?php elseif (isset($_GET['login']) && $_GET['login']=="fail"): ?>
            <span class="navbar-text"></span>
            <?php else: ?>
            <span class="navbar-text">Please log in.</span>
            <?php endif; ?>
        </ul>

    </div>

    <form class="form-inline float-sm-right" style="margin-bottom: 0;" action="/includes/login-inc.php" method="post">
        <div class="input-group input-group-sm">
            <?php if (isset($_GET['login']) && $_GET['login']=="empty"): ?>
            <input class="form-control border border-danger" type="text" name="userName" placeholder="username">
            <input class="form-control border border-danger" type="password" name="password" placeholder="password">
            <?php else: ?>
            <input class="form-control" type="text" name="userName" placeholder="username">
            <input class="form-control" type="password" name="password" placeholder="password">
            <?php endif; ?>
            <div class="input-group-append">
                <button class="btn btn-success" name="login" type="submit">login</button>
                <a href="signup.php" class="btn btn-primary" role="button">register</a>
            </div>
        </div>
    </form>
</nav>

<?php endif; ?>