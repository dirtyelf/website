<?php 
$currentYear = date("Y");
?>

<div id="footer" class="jumbotron">
      <h1>Thanks for visiting!</h1>
      <p>&copy; <?php echo($currentYear)?></p>
</div>