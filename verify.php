<?php

require 'includes/db-inc.php';
session_start();

$errorMSG = "";

// check to see if the email/hash vars are empty
if (isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])) {
    $email = mysqli_real_escape_string($connection, $_GET['email']);
    $hash = mysqli_real_escape_string($connection, $_GET['hash']);

    // find user with matching email and hash
    $sql = "SELECT * FROM users WHERE user_email='$email' AND user_hash='$hash' AND user_active='0'";
    $result = mysqli_query($connection, $sql);
    $resultCheck = mysqli_num_rows($result);

    //already activated or url invalid
    if ($resultCheck != 1) { 
        $errorMSG = "Invalid user or already activated. Your account was not activated.";
    }
    // set user to active state in DB and create last page entry
    else {
        $row = mysqli_fetch_assoc($result);
        $uID = $row['user_id'];
        $sql = "UPDATE users SET user_active='1' WHERE user_email='$email'";
        $result = mysqli_query($connection, $sql);
        if ($result == false) {
            $errorMSG = "Your account was not activated. Database error [users].";
        } 
        $sql = "INSERT INTO last_page (user_id) VALUES ($uID);";
        $result = mysqli_query($connection, $sql);
        if ($result == false) {
            $errorMSG = "Your account is activated, however there was an error.<br>";
            $errorMSG .= "Database error [page].";
        } 
        if ($errorMSG == "") {
            session_unset();
            session_destroy();
            echo('Congrats, your account is activated.<br>
                <a href="index.php">Home</a>');
            header("refresh:5; url=https://www.lindsayandbryan.com");
        } 
    }
    echo($errorMSG);
    header("refresh:5; url=https://www.lindsayandbryan.com");
} 
else {
    header("Location: index.php");
}
?>