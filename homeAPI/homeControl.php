<?php

require '../includes/db-inc.php';

$entityBody = file_get_contents('php://input');
/*
$filename = '/var/tmp/httpHeaderReqs/httpheader.txt';
file_put_contents($filename, $entityBody);
//die('logged');
*/

$varsIFTTT = explode('&', $entityBody);
$varsIFTTThash = explode('=', $varsIFTTT[0]); 
$varsIFTTTroom = explode('=', $varsIFTTT[1]); 
$varsIFTTTcommand = explode('=', $varsIFTTT[2]);
$response = '';

/*
file_put_contents($filename, $entityBody . chr(0x0D).chr(0x0A));
file_put_contents($filename, $varsIFTTT0[1] . chr(0x0D).chr(0x0A), FILE_APPEND);
file_put_contents($filename, $varsIFTTT1[1] . chr(0x0D).chr(0x0A), FILE_APPEND);
*/

if (isset($_POST)) {

    $hash = mysqli_real_escape_string($connection, $varsIFTTThash[1]);
    $sql = "SELECT user_role FROM users WHERE user_hash='$hash'";
    $result = mysqli_query($connection, $sql);
    $row = mysqli_fetch_assoc($result);
    
    /*
    file_put_contents($filename, $sql . chr(0x0D).chr(0x0A), FILE_APPEND);
    file_put_contents($filename, 'result: ' , FILE_APPEND);
    file_put_contents($filename, $row['user_role'] . chr(0x0D).chr(0x0A), FILE_APPEND);
    */

    if ($row['user_role'] == 'admin' || $row['user_role'] == 'resident'){
        if ($varsIFTTTcommand[1] != 'on-off' || $varsIFTTTcommand[1] != 'dim') {
            $fanStatus = mysqli_real_escape_string($connection, $varsIFTTTcommand[1]);

            if($fanStatus == "high" || $fanStatus == "hi"){
                if($varsIFTTTroom[1] == "living") {
                    $curl = curl_init('http://192.168.1.175/fanHi');
                } else {
                    $curl = curl_init('http://192.168.1.176/fanHi');
                }
                curl_setopt($curl, CURLOPT_PORT, 18008);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
                $response = curl_exec($curl);
                curl_close($curl);
            } 
            elseif($fanStatus == "medium"){
                if($varsIFTTTroom[1] == "living") {
                    $curl = curl_init('http://192.168.1.175/fanMed');
                } else {
                    $curl = curl_init('http://192.168.1.176/fanMed');
                }
                curl_setopt($curl, CURLOPT_PORT, 18008);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
                $response = curl_exec($curl);
                curl_close($curl);
            } 
            elseif($fanStatus == "low"){
                if($varsIFTTTroom[1] == "living") {
                    $curl = curl_init('http://192.168.1.175/fanLow');
                } else {
                    $curl = curl_init('http://192.168.1.176/fanLow');
                }
                curl_setopt($curl, CURLOPT_PORT, 18008);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
                $response = curl_exec($curl);
                curl_close($curl);
            } 
            elseif($fanStatus == "off"){
                if($varsIFTTTroom[1] == "living") {
                    $curl = curl_init('http://192.168.1.175/fanOff');
                } else {
                    $curl = curl_init('http://192.168.1.176/fanOff');
                }
                curl_setopt($curl, CURLOPT_PORT, 18008);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
                $response = curl_exec($curl);
                curl_close($curl);
            }
        }
        
        if ($varsIFTTTcommand[1] == 'on-off' || $varsIFTTTcommand[1] == 'dim') {
            $lightStatus = mysqli_real_escape_string($connection, $varsIFTTTcommand[1]);
            if($lightStatus == "on-off") {
                if($varsIFTTTroom[1] == "living") {
                    $curl = curl_init('http://192.168.1.175/light');
                } else {
                    $curl = curl_init('http://192.168.1.176/light');
                }
                curl_setopt($curl, CURLOPT_PORT, 18008);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
                $response = curl_exec($curl);
                curl_close($curl);
            } 
            elseif($lightStatus == "dim") {
                if($varsIFTTTroom[1] == "living") {
                    $curl = curl_init('http://192.168.1.175/dim');
                } else {
                    $curl = curl_init('http://192.168.1.176/dim');
                }
                curl_setopt($curl, CURLOPT_PORT, 18008);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 18008);
                $response = curl_exec($curl);
                curl_close($curl);
            }
        }
    } else {
        header('Location: ../index.php');
        //die('not allowed:1');
    }
} else {
    header('Location: ../index.php');
    //die('not allowed:2');
}